<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
    <button><a href="/servletMVC/LoginController">Back</a></button>
	<form action="/servletMVC/edit" method="post">
	    <input type="hidden" name="id" value="${requestScope['data'].id}">
		Enter Name : <input type="text" name="name" value="${requestScope['data'].fullname}"> <BR>
		Enter Email : <input type="email" name="email" value="${requestScope['data'].email}"> <BR>
		Enter Password : <input type="password" name="password"> <BR>
		Enter address : <input type="text" name="address" value="${requestScope['data'].address}"> <BR>
		Enter status :      Before:      <input type="text" readonly value="${requestScope['data'].status}">
		                    <select name="status">
		                    <option selected disabled>Choose</option>
		                    <option value="Active">Active</option>
		                    <option value="Non Active">Non Active</option>
		                    <option value="On Leave">On Leave</option>
		               </select>
		<BR>
		Enter physics : <input value="${requestScope['data'].physics}" type="number" name="physics"> <BR>
		Enter calculus : <input value="${requestScope['data'].calculus}" type="number" name="calculus"> <BR>
		Enter biologi : <input value="${requestScope['data'].biologi}" type="number" name="biologi"> <BR>
		<input type="submit" value="Save"/>
	</form>
</body>
</html>
